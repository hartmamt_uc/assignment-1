//
//  CalculatorViewController.h
//  Calculator
//
//  Created by Matt Hartman on 10/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalculatorViewController : UIViewController

@property (retain, nonatomic) IBOutlet UILabel *display;
@property (retain, nonatomic) IBOutlet UILabel *brainDisplay;

@end
