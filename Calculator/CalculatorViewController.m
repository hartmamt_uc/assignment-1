//
//  CalculatorViewController.m
//  Calculator
//
//  Created by Matt Hartman on 10/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CalculatorViewController.h"
#import "CalculatorBrain.h"

@interface CalculatorViewController ()

@property (nonatomic) BOOL userIsInMiddleOfEnteringANumber;
@property (nonatomic, strong) CalculatorBrain *brain;

@end

@implementation CalculatorViewController

@synthesize display;
@synthesize brainDisplay;
@synthesize userIsInMiddleOfEnteringANumber;
@synthesize brain = _brain;

- (CalculatorBrain *)brain
{
    if (!_brain) _brain = [[CalculatorBrain alloc] init];
    return _brain;
}

- (IBAction)digitPressed:(UIButton *)sender {
    NSString *digit= [sender currentTitle];

    if (self.userIsInMiddleOfEnteringANumber) {
        if ([@"." isEqualToString:digit])
        {
            if ( [self.display.text rangeOfString: @"."].location ==NSNotFound ) {
                self.display.text=[self.display.text stringByAppendingString:digit];   
            }
            
        } else {
            self.display.text=[self.display.text stringByAppendingString:digit];
        }
        
    } else {
        self.display.text = digit;
        self.userIsInMiddleOfEnteringANumber = YES;
    }
}
- (IBAction)clearPressed {
    [self.brain clearOperandStack];
    self.display.text = @"0";
    self.brainDisplay.text = @"";
}

- (IBAction)enterPressed {
    [self.brain pushOperand:[self.display.text doubleValue]];
    self.userIsInMiddleOfEnteringANumber = NO;
    self.brainDisplay.text = [self.brainDisplay.text stringByAppendingString:self.display.text];
    self.brainDisplay.text = [self.brainDisplay.text stringByAppendingString:@" "];
}

- (IBAction)operationPressed:(UIButton *)sender {
    if (self.userIsInMiddleOfEnteringANumber)
    {
        [self enterPressed];
    }
    NSString *operation = [sender currentTitle];
    
    self.brainDisplay.text = [self.brainDisplay.text stringByAppendingString:operation];
    self.brainDisplay.text = [self.brainDisplay.text stringByAppendingString:@" "];

    double result = [self.brain performOperation:operation];
    self.display.text = [NSString stringWithFormat:@"%g", result];
}


@end
